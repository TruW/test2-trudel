/**@author William
 * 
 */
package Planets;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		ArrayList<Planet> inner=new ArrayList<Planet>(planets);
		for(int i=0;i<inner.size();i++) {
			if(inner.get(i).getOrder()>=4)
				inner.remove(i);
		}
		return inner;
	}
}
