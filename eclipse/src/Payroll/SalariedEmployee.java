/**@author William
 * 
 */
package Payroll;

public class SalariedEmployee implements Employee{
	private double yearlySalary;
	
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary=yearlySalary;
	}

	@Override
	public double getYearlyPay() {
		return yearlySalary;
	}	
}
