/**@author William
 * 
 */
package Payroll;

public class HourlyEmployee implements Employee {
	private int hoursWeekly;
	private double hourlyPay;
	
	public HourlyEmployee(int hoursWeekly,double hourlyPay) {
		this.hoursWeekly=hoursWeekly;
		this.hourlyPay=hourlyPay;
	}

	@Override
	public double getYearlyPay() {
		return hourlyPay*hoursWeekly*52;
	}

}
