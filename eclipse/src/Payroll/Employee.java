/**@author William
 * 
 */
package Payroll;

public interface Employee {
	public double getYearlyPay();
}
