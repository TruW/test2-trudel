/**@author William
 * 
 */
package Payroll;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double pensionContribution;
	
	public UnionizedHourlyEmployee(int hoursWeekly,double hourlyPay,double pensionContribution) {
		super(hoursWeekly,hourlyPay);
		this.pensionContribution=pensionContribution;
	}
	
	@Override
	public double getYearlyPay() {
		return super.getYearlyPay()+pensionContribution;
	}
}
