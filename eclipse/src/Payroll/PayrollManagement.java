/**@author William
 * 
 */
package Payroll;

public class PayrollManagement {

	public static void main(String[] args) {
		Employee[] e= {new SalariedEmployee(66000),new SalariedEmployee(58000),new SalariedEmployee(97000),new HourlyEmployee(45,12.50),new UnionizedHourlyEmployee(15,12.50,5000)};
		System.out.println(getTotalExpenses(e));
	}
	public static double getTotalExpenses(Employee[] e) {
		double total=0;
		for(int i=0;i<e.length;i++)
			total+=e[i].getYearlyPay();
		return total;
	}
}
